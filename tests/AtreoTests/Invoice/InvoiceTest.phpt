<?php

namespace AtreoTests\Invoice;

use Atreo\Invoice\Data;
use Atreo\Invoice\Invoice;
use Atreo\Invoice\PaymentDetails;
use Atreo\Invoice\Subject;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class InvoiceTest extends TestCase
{

    public function testGenerateInvoice()
    {
		$customer = new Subject('Jon Doe');
		$supplier = new Subject('Doe Jon');
		$paymentDetails = new PaymentDetails('123456', '123');
		$data = new Data(1, $paymentDetails, $customer, $supplier, new \DateTime(), new \DateTime('+ 7 days'));

		$filePath = __DIR__ . '/faktura.pdf';
		$invoice = new Invoice($data);
		$invoice->exportToFile($filePath);

		Assert::true(is_file($filePath));
    }

}

\run(new InvoiceTest());
