Atreo/Invoice
===========================


Requirements
------------

Atreo/Invoice requires PHP 5.4.*.

- [Nette Framework](https://github.com/nette/nette)
- [mPdf](https://github.com/finwe/mpdf)


Installation
------------

The best way to install Atreo/Inovice is using  [Composer](http://getcomposer.org/):

```sh
$ composer require atreo/invoice
```
