<?php

namespace Atreo\Invoice;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 *
 * @property string $id
 * @property string $typeName
 * @property PaymentDetails $paymentDetails
 * @property Subject $customer
 * @property Subject $supplier
 * @property \DateTime $dateOfExpiration
 * @property \DateTime $dateOfIssuance
 * @property \DateTime $dateOfVatRevenueRecognition
 * @property array $items
 * @property-read bool $isVatUsed
 * @property-read float $totalPriceWithVat
 * @property-read float $totalPriceWithoutVat
 * @property-read float $totalVat
 */
class Data extends Object
{

	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $typeName = 'Faktura';

	/**
	 * @var PaymentDetails
	 */
	private $paymentDetails;

	/**
	 * @var Subject
	 */
	private $customer;

	/**
	 * @var Subject
	 */
	private $supplier;

	/**
	 * @var \DateTime
	 */
	private $dateOfExpiration;

	/**
	 * @var \DateTime
	 */
	private $dateOfIssuance;

	/**
	 * @var \DateTime
	 */
	private $dateOfVatRevenueRecognition;

	/**
	 * @var float
	 */
	private $totalPriceWithVat;

	/**
	 * @var float
	 */
	private $totalPriceWithoutVat;

	/**
	 * @var float
	 */
	private $totalVat;

	/**
	 * @var bool
	 */
	private $isVatUsed = TRUE;

	/**
	 * @var Item[]
	 */
	private $items = [];



	/**
	 * @param string $id
	 * @param PaymentDetails $paymentDetails
	 * @param Subject $customer
	 * @param Subject $supplier
	 * @param \DateTime $dateOfIssuance
	 * @param \DateTime|NULL $dateOfExpiration
	 */
	public function __construct($id, PaymentDetails $paymentDetails, Subject $customer, Subject $supplier, \DateTime $dateOfIssuance, \DateTime $dateOfExpiration = NULL)
	{
		$this->id = $id;
		$this->paymentDetails = $paymentDetails;
		$this->customer = $customer;
		$this->supplier = $supplier;
		$this->dateOfIssuance = $dateOfIssuance;

		if ($dateOfExpiration) {
			$this->dateOfExpiration = $dateOfExpiration;
		} else {
			$this->dateOfExpiration = clone $dateOfIssuance;
			$this->dateOfExpiration->modify('+ 7 days');
		}

		$this->dateOfVatRevenueRecognition = $this->dateOfIssuance;
	}



	/**
	 * @param \DateTime $dateOfExpiration
	 */
	public function setDateOfExpiration(\DateTime $dateOfExpiration)
	{
		$this->dateOfExpiration = $dateOfExpiration;
	}



	/**
	 * @return \DateTime
	 */
	public function getDateOfExpiration()
	{
		return $this->dateOfExpiration;
	}



	/**
	 * @param \DateTime $dateOfIssuance
	 */
	public function setDateOfIssuance(\DateTime $dateOfIssuance)
	{
		$this->dateOfIssuance = $dateOfIssuance;
	}



	/**
	 * @return \DateTime
	 */
	public function getDateOfIssuance()
	{
		return $this->dateOfIssuance;
	}



	/**
	 * @param \DateTime $dateOfVatRevenueRecognition
	 */
	public function setDateOfVatRevenueRecognition(\DateTime $dateOfVatRevenueRecognition)
	{
		$this->dateOfVatRevenueRecognition = $dateOfVatRevenueRecognition;
	}



	/**
	 * @return \DateTime
	 */
	public function getDateOfVatRevenueRecognition()
	{
		return $this->dateOfVatRevenueRecognition;
	}



	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}



	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}



	/**
	 * @param mixed $typeName
	 */
	public function setTypeName($typeName)
	{
		$this->typeName = $typeName;
	}



	/**
	 * @return mixed
	 */
	public function getTypeName()
	{
		return $this->typeName;
	}



	/**
	 * @param Item $item
	 */
	public function addItem(Item $item)
	{
		if ($item->priceWithoutVat === NULL) {
			$this->isVatUsed = FALSE;
		}
		$this->totalPriceWithVat += $item->priceWithVat;
		$this->totalPriceWithoutVat += $item->priceWithoutVat;
		$this->totalVat += $item->vat;
		$this->items[] = $item;
	}



	/**
	 * @return \Atreo\Invoice\Item[]
	 */
	public function getItems()
	{
		return $this->items;
	}



	/**
	 * @param \Atreo\Invoice\PaymentDetails $paymentDetails
	 */
	public function setPaymentDetails(PaymentDetails $paymentDetails)
	{
		$this->paymentDetails = $paymentDetails;
	}



	/**
	 * @return \Atreo\Invoice\PaymentDetails
	 */
	public function getPaymentDetails()
	{
		return $this->paymentDetails;
	}



	/**
	 * @param mixed $customer
	 */
	public function setCustomer($customer)
	{
		$this->customer = $customer;
	}



	/**
	 * @return mixed
	 */
	public function getCustomer()
	{
		return $this->customer;
	}



	/**
	 * @param \Atreo\Invoice\Subject $supplier
	 */
	public function setSupplier(Subject $supplier)
	{
		$this->supplier = $supplier;
	}



	/**
	 * @return \Atreo\Invoice\Subject
	 */
	public function getSupplier()
	{
		return $this->supplier;
	}



	/**
	 * @return float
	 */
	public function getTotalPriceWithVat()
	{
		return $this->totalPriceWithVat;
	}



	/**
	 * @return float
	 */
	public function getTotalPriceWithoutVat()
	{
		return $this->totalPriceWithoutVat;
	}



	/**
	 * @return float
	 */
	public function getTotalVat()
	{
		return $this->totalVat;
	}



	/**
	 * @return boolean
	 */
	public function getIsVatUsed()
	{
		return $this->isVatUsed;
	}


}
