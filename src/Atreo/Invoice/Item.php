<?php

namespace Atreo\Invoice;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 *
 * @property-read string $name
 * @property-read float $priceWithoutVat
 * @property-read float $priceWithVat
 * @property-read float $unitPriceWithoutVat
 * @property-read float $unitPriceWithVat
 * @property-read int $amount
 * @property-read float $vat
 * @property-read float $vatTax
 */
class Item extends Object
{

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var float
	 */
	protected $priceWithoutVat;

	/**
	 * @var float
	 */
	protected $priceWithVat;

	/**
	 * @var float
	 */
	protected $unitPriceWithoutVat;

	/**
	 * @var float
	 */
	protected $unitPriceWithVat;

	/**
	 * @var int
	 */
	protected $amount;

	/**
	 * @var float
	 */
	protected $vat;

	/**
	 * @var float
	 */
	protected $vatTax;


	/**
	 * @param string $name
	 * @param float|NULL $priceWithVat
	 * @param float|NULL $vatTax
	 * @param int $amount
	 * @throws \Exception
	 */
	public function __construct($name, $priceWithVat, $vatTax = NULL, $amount = 1)
	{
		if ($vatTax && ($vatTax < 0 || $vatTax > 100)) {
			throw new \Exception("Vat tax must be between 1 and 100.");
		}

		$this->unitPriceWithVat = $priceWithVat;
		if ($vatTax !== NULL) {
			$vat = round($priceWithVat * round($vatTax / (100 + $vatTax), 4));
			$this->unitPriceWithoutVat = $priceWithVat - $vat;
			$this->vat = $vat * $amount;
//			$this->vat = ($this->unitPriceWithVat - $this->unitPriceWithoutVat) * $amount;
			$this->priceWithoutVat = $this->unitPriceWithoutVat * $amount;
		}

		$this->vatTax = $vatTax;
		$this->amount = $amount;
		$this->name = $name;
		$this->priceWithVat = $this->unitPriceWithVat * $amount;
	}



	/**
	 * @return mixed
	 */
	public function getAmount()
	{
		return $this->amount;
	}



	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}



	/**
	 * @return mixed
	 */
	public function getPriceWithVat()
	{
		return $this->priceWithVat;
	}



	/**
	 * @return mixed
	 */
	public function getPriceWithoutVat()
	{
		return $this->priceWithoutVat;
	}



	/**
	 * @return mixed
	 */
	public function getUnitPriceWithVat()
	{
		return $this->unitPriceWithVat;
	}



	/**
	 * @return mixed
	 */
	public function getUnitPriceWithoutVat()
	{
		return $this->unitPriceWithoutVat;
	}



	/**
	 * @return float
	 */
	public function getVat()
	{
		return $this->vat;
	}



	/**
	 * @return mixed
	 */
	public function getVatTax()
	{
		return $this->vatTax;
	}

}
