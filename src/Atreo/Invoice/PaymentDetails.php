<?php

namespace Atreo\Invoice;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 *
 * @property string $accountNumber
 * @property string $bankName
 * @property string $variableSymbol
 * @property string $constantSymbol
 * @property string $specificSymbol
 * @property string $paymentType
 */
class PaymentDetails extends Object
{

	/**
	 * @var string
	 */
	private $accountNumber;

	/**
	 * @var string
	 */
	private $bankName;

	/**
	 * @var string
	 */
	private $variableSymbol;

	/**
	 * @var string
	 */
	private $constantSymbol;

	/**
	 * @var string
	 */
	private $specificSymbol;

	/**
	 * @var string
	 */
	private $paymentType;



	/**
	 * @param string $accountNumber
	 * @param string $variableSymbol
	 */
	public function __construct($accountNumber, $variableSymbol)
	{
		$this->accountNumber = $accountNumber;
		$this->variableSymbol = $variableSymbol;
	}



	/**
	 * @param string $accountNumber
	 */
	public function setAccountNumber($accountNumber)
	{
		$this->accountNumber = $accountNumber;
	}



	/**
	 * @return string
	 */
	public function getAccountNumber()
	{
		return $this->accountNumber;
	}



	/**
	 * @param string $bankName
	 */
	public function setBankName($bankName)
	{
		$this->bankName = $bankName;
	}



	/**
	 * @return string
	 */
	public function getBankName()
	{
		return $this->bankName;
	}



	/**
	 * @param string $constantSymbol
	 */
	public function setConstantSymbol($constantSymbol)
	{
		$this->constantSymbol = $constantSymbol;
	}



	/**
	 * @return string
	 */
	public function getConstantSymbol()
	{
		return $this->constantSymbol;
	}



	/**
	 * @param string $specificSymbol
	 */
	public function setSpecificSymbol($specificSymbol)
	{
		$this->specificSymbol = $specificSymbol;
	}



	/**
	 * @return string
	 */
	public function getSpecificSymbol()
	{
		return $this->specificSymbol;
	}



	/**
	 * @param string $paymentType
	 */
	public function setPaymentType($paymentType)
	{
		$this->paymentType = $paymentType;
	}



	/**
	 * @return string
	 */
	public function getPaymentType()
	{
		return $this->paymentType;
	}



	/**
	 * @param string $variableSymbol
	 */
	public function setVariableSymbol($variableSymbol)
	{
		$this->variableSymbol = $variableSymbol;
	}



	/**
	 * @return string
	 */
	public function getVariableSymbol()
	{
		return $this->variableSymbol;
	}

}
