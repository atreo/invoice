<?php

namespace Atreo\Invoice;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 *
 * @property string $name
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $zipCode
 * @property string $in
 * @property string $tin
 */
class Subject extends Object
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $street;

	/**
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 */
	private $state;

	/**
	 * @var string
	 */
	private $zipCode;

	/**
	 * @var string
	 */
	private $in;

	/**
	 * @var string
	 */
	private $tin;



	/**
	 * @param string $name
	 */
	public function __construct($name)
	{
		$this->name = $name;
	}



	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}



	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}



	/**
	 * @param string $in
	 */
	public function setIn($in)
	{
		$this->in = $in;
	}



	/**
	 * @return string
	 */
	public function getIn()
	{
		return $this->in;
	}



	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}



	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}



	/**
	 * @param string $state
	 */
	public function setState($state)
	{
		$this->state = $state;
	}



	/**
	 * @return string
	 */
	public function getState()
	{
		return $this->state;
	}



	/**
	 * @param string $street
	 */
	public function setStreet($street)
	{
		$this->street = $street;
	}



	/**
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}



	/**
	 * @param string $tin
	 */
	public function setTin($tin)
	{
		$this->tin = $tin;
	}



	/**
	 * @return string
	 */
	public function getTin()
	{
		return $this->tin;
	}



	/**
	 * @param string $zipCode
	 */
	public function setZipCode($zipCode)
	{
		$this->zipCode = $zipCode;
	}



	/**
	 * @return string
	 */
	public function getZipCode()
	{
		return $this->zipCode;
	}

}
