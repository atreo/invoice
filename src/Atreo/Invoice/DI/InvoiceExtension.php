<?php

namespace Atreo\Invoice\DI;

use Atreo\Invoice\Invoice;
use Nette\Configurator;
use Nette\DI\CompilerExtension;
use Nette\DI\Compiler;
use Nette\Utils\Validators;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class InvoiceExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();
		$builder->addDefinition($this->prefix('client'))
			->setClass('Atreo\Invoice\Invoice')
			->setArguments(array('@application', '@httpRequest'));
	}



	/**
	 * @param \Nette\Configurator $configurator
	 */
	public static function register(Configurator $configurator)
	{
		$configurator->onCompile[] = function ($config, Compiler $compiler) {
			$compiler->addExtension('invoice', new InvoiceExtension());
		};
	}

}
