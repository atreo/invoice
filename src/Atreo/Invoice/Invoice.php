<?php

namespace Atreo\Invoice;

use Nette\Application\UI\Control;
use Nette\Application\UI\ITemplate;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 *
 * @property Data $data
 * @property string $templatePath
 * @property string $stampPath
 * @property string $logoPath
 * @property string $color
 */
class Invoice extends Control
{

	/**
	 * @var Data
	 */
	private $data;

	/**
	 * @var ITemplate
	 */
	private $template;

	/**
	 * @var string
	 */
	private $templatePath;

	/**
	 * @var string
	 */
	private $stampPath;

	/**
	 * @var string
	 */
	private $logoPath;

	/**
	 * @var string
	 */
	private $color = '#000000';

	/**
	 * @var \mPDF
	 */
	private $mpdf;

	/**
	 * @var bool
	 */
	private $isPrepared = FALSE;




	public function __construct(ITemplate $template, Data $data)
	{
		$this->template = $template;
		$this->data = $data;
	}



	/**
	 * @param string $name
	 */
	public function openPdf($name)
	{
		$this->prepare();
		$this->mpdf->Output($this->getInvoiceFileName($name), 'I');
	}



	/**
	 * @param string|NULL $name
	 */
	public function downloadPdf($name = NULL)
	{
		$this->prepare();
		$this->mpdf->Output($this->getInvoiceFileName($name), 'D');
	}



	/**
	 * @param string $path
	 */
	public function exportToFile($path)
	{
		$this->prepare();
		$this->mpdf->Output($path, 'F');
	}



	/**
	 * @return string
	 */
	public function getAsString()
	{
		$this->prepare();
		return $this->mpdf->Output('', 'S');
	}



	/**
	 * @param string|NULL $name
	 * @return string
	 */
	public function getInvoiceFileName($name = NULL)
	{
		return $name ? $name : Strings::webalize($this->data->typeName . ' ' . $this->data->id) . '.pdf';
	}



	public function prepare(\mPDF $mpdf = NULL)
	{
		if ($this->isPrepared) {
			return;
		}
		$this->isPrepared = TRUE;

		if ($this->templatePath) {
			$this->template->setFile($this->templatePath);
		} else {
			$this->template->setFile(__DIR__ . '/invoice.latte');
		}
		$this->template->id = $this->data->id;
		$this->template->typeName = $this->data->typeName;
		$this->template->paymentDetails = $this->data->paymentDetails;
		$this->template->customer = $this->data->customer;
		$this->template->supplier = $this->data->supplier;
		$this->template->dateOfExpiration = $this->data->dateOfExpiration;
		$this->template->dateOfIssuance = $this->data->dateOfIssuance;
		$this->template->dateOfVatRevenueRecognition = $this->data->dateOfVatRevenueRecognition;
		$this->template->items = $this->data->items;
		$this->template->isVatUsed = $this->data->isVatUsed;
		$this->template->totalPriceWithVat = $this->data->totalPriceWithVat;
		$this->template->totalPriceWithoutVat = $this->data->totalPriceWithoutVat;
		$this->template->totalVat = $this->data->totalVat;
		$this->template->stampPath = $this->stampPath;
		$this->template->logoPath = $this->logoPath;
		$this->template->color = $this->color;

		if (!$mpdf) {
			$mpdf = new \mPDF('utf-8');
		}
		$this->mpdf = $mpdf;
		$mpdf->AddPage();
		$mpdf->WriteHTML((string) $this->template);
	}



	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}



	/**
	 * @param string $templatePath
	 */
	public function setTemplatePath($templatePath)
	{
		$this->templatePath = $templatePath;
	}



	/**
	 * @return string
	 */
	public function getTemplatePath()
	{
		return $this->templatePath;
	}



	/**
	 * @param string $stampPath
	 */
	public function setStampPath($stampPath)
	{
		$this->stampPath = $stampPath;
	}



	/**
	 * @return string
	 */
	public function getStampPath()
	{
		return $this->stampPath;
	}



	/**
	 * @param string $logoPath
	 */
	public function setLogoPath($logoPath)
	{
		$this->logoPath = $logoPath;
	}



	/**
	 * @return string
	 */
	public function getLogoPath()
	{
		return $this->logoPath;
	}



	/**
	 * @return string
	 */
	public function getColor()
	{
		return $this->color;
	}



	/**
	 * @param string $color
	 */
	public function setColor($color)
	{
		$this->color = $color;
	}

}



class InvoiceException extends \RuntimeException {}
